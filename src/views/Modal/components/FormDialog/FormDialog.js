import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { 
  Button,
  TextField,
  FormControlLabel,
  Radio,
  RadioGroup,
  Divider,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
 } from '@material-ui/core';
 import html2canvas from 'html2canvas';

 import { debugLog } from '../../../../helpers/utils';

const useStyles = makeStyles(theme => ({
  root: {},
  padded: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  }
}));

const FormDialog = props => {
  const { title, dialogType } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    // const selector = document.querySelector("*[data-id='test']");
    // const selector = document.querySelector("*[data-capture-id='test-dialog']");
    // html2canvas(selector)

    html2canvas(document.body)
    .then(canvas => {
      // const dataURL = canvas.toDataURL();
      document.body.appendChild(canvas);
    })
    .catch(err => {
      debugLog(`Error capturing screenshot. Check browser support. ${err}`);
    });
    setOpen(false);
  };

  const classes = useStyles();

  const searchDialog = () => {
    return (
     <Dialog
     aria-labelledby="form-dialog-title"
     onClose={handleClose}
     open={open}
   >
      <DialogTitle id="form-dialog-title">Search</DialogTitle>
      <Divider />
      <DialogContent>
        <DialogContentText>
          To subscribe to this website, please enter your email address here.
          We will send updates occasionally.
        </DialogContentText>
        <TextField
         autoFocus
         fullWidth
         id="search"
         label="Search Query"
         margin="dense"
         type="input"
         />
     </DialogContent>
     <DialogActions>
       <Button variant="outlined" color="secondary" onClick={handleClose}>
         Cancel
       </Button>
       <Button
         color="primary"
         variant="contained"
         onClick={handleClose}
         >
         Search
       </Button>
     </DialogActions>
   </Dialog>
   
    )
   }

  const feedbackDialog = () => {

    return (
      <Dialog
      aria-labelledby="form-dialog-title"
      onClose={handleClose}
      open={open}
      data-capture-id="test-dialog"
      id="test-dialog"
      data-html2canvas-ignore={true}
      >
     <DialogTitle id="form-dialog-title">Submit Search Relevance Feedback</DialogTitle>
     <Divider />
     <DialogContent>
        <DialogContentText>
         Please submit feedback on the relevance of this search result and provide any other relevant infomation in the form below.
        </DialogContentText>
        <RadioGroup aria-label="feedback" name="feedback" className={classes.padded}>
          <FormControlLabel value="1" control={<Radio color="primary"/>} label="This is relavant" />
          <FormControlLabel value="2" control={<Radio color="primary"/>} label="This isn't relevant" />
          <FormControlLabel value="3" control={<Radio color="primary"/>} label="Something is wrong" />
          <FormControlLabel value="4" control={<Radio color="primary"/>} label="This isn't useful" />
        </RadioGroup>
       <TextField
         autoComplete="off"
         fullWidth
         id="search"
         label="Additional Comments?"
         variant="outlined"
         type="input"
         multiline
         rows="3"
         />  
     </DialogContent>
     <DialogActions>
       <Button variant="outlined" color="secondary" onClick={handleClose}>
         Cancel
       </Button>
       <Button
         color="primary"
         variant="contained"
         onClick={handleClose}
       >
         Submit
       </Button>
     </DialogActions>
     </Dialog> 
    )
   } 

  return (
    <div className={classes.root}>
      <Button
        color="primary"
        onClick={handleClickOpen}
        variant="outlined"
      >
        { title }
      </Button>
      { dialogType === 'search' ? searchDialog() : feedbackDialog() }
     </div>
  );
};

export default FormDialog;
