import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Typography, Button, Card } from '@material-ui/core';

import { SimpleDialog, FormDialog, AlertDialog, FeedbackForm } from './components';

const emails = ['username@gmail.com', 'user02@gmail.com'];
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    margin: theme.spacing(3)
  }
}));

const Modal = () => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(emails[1]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = value => {
    setOpen(false);
    setSelectedValue(value);
  };

  return (
    <div>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <Card className={classes.root} data-id="test">
            <Typography variant="subtitle1">
              Selected: {selectedValue}
            </Typography>
            <br />
            <Button
              color="primary"
              onClick={handleClickOpen}
              variant="outlined"
            >
              Open simple dialog
            </Button>
            <SimpleDialog
              onClose={handleClose}
              open={open}
              selectedValue={selectedValue}
            />
          </Card>
        </Grid>
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <Card className={classes.root}>
            <FormDialog title={"Open Search Dialog"} dialogType={'search'}/>
          </Card>
        </Grid>
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <Card className={classes.root}>
            <FormDialog title={"Open Feedback Dialog"} dialogType={'feedback'}/>
          </Card>
        </Grid>
        <Grid
          item
          lg={4}
          md={6}
          xl={4}
          xs={12}
        >
          <Card className={classes.root}>
            <AlertDialog />
          </Card>
        </Grid>
        <Grid
          item
          xl={4}
          lg={8}
          md={6}
          xs={12}
        >
          <FeedbackForm className={classes.root}  />
        </Grid>
      </Grid>
    </div>
  );
};

export default Modal;
