import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/styles';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  root: {}
}));

const AccountActions = props => {

  const { title } = props;

  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    enqueueSnackbar('Dialog Closed', { 
      variant: 'success',
    });
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button
        color="primary"
        onClick={handleClickOpen}
        variant="outlined"
      >
        {title}
      </Button>
      <Dialog
        aria-labelledby="form-dialog-title"
        onClose={handleClose}
        open={open}
      >
        <DialogTitle id="form-dialog-title">Search</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter your search query
          </DialogContentText>
          <TextField
            autoFocus
            fullWidth
            id="search"
            label="Search Query"
            margin="dense"
            type="input"
          />
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" color="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={handleClose}
          >
            Search
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

AccountActions.propTypes = {
  title: PropTypes.string
};

export default AccountActions;
