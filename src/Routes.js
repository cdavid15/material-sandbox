import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  Modal as ModalView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from={`${process.env.PUBLIC_URL}/`}
        to={`${process.env.PUBLIC_URL}/dashboard`}
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/dashboard`}
      />
      <RouteWithLayout
        component={UserListView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/users`}
      />
      <RouteWithLayout
        component={ProductListView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/products`}
      />
      <RouteWithLayout
        component={TypographyView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/typography`}
      />
      <RouteWithLayout
        component={IconsView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/icons`}
      />
      <RouteWithLayout
        component={AccountView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/account`}
      />
      <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/settings`}
      />
      <RouteWithLayout
        component={ModalView}
        exact
        layout={MainLayout}
        path={`${process.env.PUBLIC_URL}/modal`}
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path={`${process.env.PUBLIC_URL}/sign-up`}
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path={`${process.env.PUBLIC_URL}/sign-in`}
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path={`${process.env.PUBLIC_URL}/not-found`}
      />
      <Redirect to={`${process.env.PUBLIC_URL}/not-found`} />
    </Switch>
  );
};

export default Routes;
