export const debugLog = (...args) => {
    if (process.env.NODE_ENV !== 'production') {
      return console.log('[Sandbox]', ...args);
    }
  };